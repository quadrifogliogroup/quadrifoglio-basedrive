#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "quadrifoglio_msgs/baseDriveCmd.h"

#include <cmath>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

const double wheelbase = 25.3;  //Robot wheelbase and track in cm, for ackerman angle calculations
const double track = 15.5;
const double maxSpeed = 1.6; //Maximum robot speed in m/s

template <class T>
T Sign (T a) {
  T result;
  result = (a<0.0)? -1 : 1;
  return (result);
}

ros::Publisher baseDrivePub;
quadrifoglio_msgs::baseDriveCmd driveCmd;

double maxLinAccX = 1.5; //In m/s²
double maxLinAccY = 1.0; //In m/s²
double maxRotAccZ = 4.0; //In rad/s²

//This approaches the commanded value with the set acceleration values
geometry_msgs::Twist limOutTwist;  
ros::Time cmdVelTimestamp;


//Calculates both wheel angles and speeds based on a twist message with additive vectors
void VectorDrive(quadrifoglio_msgs::baseDriveCmd& driveCmd, geometry_msgs::Twist& twistMsg) {

  //Distance of wheels from the centerpoint, assumes symmetric robot
  double distFromCenter = std::sqrt(std::pow(wheelbase/2.0,2.0)+pow(track/2.0,2.0))/100.0;  //in meters
  double rotMagn = twistMsg.angular.z*distFromCenter;  //Magnitude of rotational velocity component

  double ulVectAngle = std::atan2(track/2,-wheelbase/2);  //angles for rotational velocity component in rads
  double urVectAngle = std::atan2(track/2,wheelbase/2);
  double dlVectAngle = std::atan2(-track/2,-wheelbase/2);
  double drVectAngle = std::atan2(-track/2,wheelbase/2);

  double ulVelX = twistMsg.linear.x - sin(ulVectAngle)*rotMagn;
  double ulVelY = -twistMsg.linear.y + cos(ulVectAngle)*rotMagn;
  
  double urVelX = twistMsg.linear.x + sin(urVectAngle)*rotMagn;
  double urVelY = -twistMsg.linear.y - cos(urVectAngle)*rotMagn;
  
  double dlVelX = twistMsg.linear.x + sin(dlVectAngle)*rotMagn;
  double dlVelY = -twistMsg.linear.y - cos(dlVectAngle)*rotMagn;
  
  double drVelX = twistMsg.linear.x - sin(drVectAngle)*rotMagn;
  double drVelY = -twistMsg.linear.y + cos(drVectAngle)*rotMagn;

  //actual wheel angle from the resulting velocity vector
  double upLeftAngle = std::atan2(ulVelY,ulVelX); 
  double upRightAngle = std::atan2(urVelY,urVelX);
  double downLeftAngle = std::atan2(dlVelY,dlVelX);
  double downRightAngle = std::atan2(drVelY,drVelX);

  driveCmd.frontLeftMot = std::sqrt(std::pow(ulVelX,2.0)+pow(ulVelY,2.0))*Sign(ulVelX);
  driveCmd.frontRightMot = std::sqrt(std::pow(urVelX,2.0)+pow(urVelY,2.0))*Sign(urVelX);
  driveCmd.rearLeftMot = std::sqrt(std::pow(dlVelX,2.0)+pow(dlVelY,2.0))*Sign(dlVelX);
  driveCmd.rearRightMot = std::sqrt(std::pow(drVelX,2.0)+pow(drVelY,2.0))*Sign(drVelX);

  //Get angles within +-90 (-100 becomes 80 and so on) and flip wheel speeds to match
  if(std::abs(upLeftAngle) > 90.0*TORAD){
    if(upLeftAngle > 0) upLeftAngle = upLeftAngle - 180.0*TORAD;
    else upLeftAngle = upLeftAngle + 180.0*TORAD;
  }
  if(std::abs(upRightAngle) > 90.0*TORAD){
    if(upRightAngle > 0) upRightAngle = upRightAngle - 180.0*TORAD;
    else upRightAngle = upRightAngle + 180.0*TORAD;
  }
  if(std::abs(downLeftAngle) > 90.0*TORAD){
    if(downLeftAngle > 0) downLeftAngle = downLeftAngle - 180.0*TORAD;
    else downLeftAngle = downLeftAngle + 180.0*TORAD;
  }
  if(std::abs(downRightAngle) > 90.0*TORAD){
    if(downRightAngle > 0) downRightAngle = downRightAngle - 180.0*TORAD;
    else downRightAngle = downRightAngle + 180.0*TORAD;
  }

  //Fill and publish low level basedrive message
  driveCmd.frontLeftServo = upLeftAngle*TODEG; driveCmd.frontRightServo = upRightAngle*TODEG;
  driveCmd.rearLeftServo = downLeftAngle*TODEG; driveCmd.rearRightServo = downRightAngle*TODEG;

  baseDrivePub.publish(driveCmd);

  //ROS_INFO("ulVect %f dist %f rotmagn %f",ulVectAngle,distFromCenter, rotMagn);
}

void TwistTowardTwist(const geometry_msgs::Twist::ConstPtr& in, geometry_msgs::Twist& out){

    //if(ros::Time::now()-cmdVelTimestamp > ros::Duration(0.25))

    double secondsPassed = (ros::Time::now()-cmdVelTimestamp).toSec();  //This many seconds secondsPassed
    cmdVelTimestamp = ros::Time::now();

    double maxLinXStep = maxLinAccX * secondsPassed;  //How much the values can increase in this step
    double maxLinYStep = maxLinAccY * secondsPassed;
    double maxRotZStep = maxRotAccZ * secondsPassed;

    ROS_INFO_THROTTLE(1,"timestep %f maxlinx %f", secondsPassed, maxLinXStep);

    //If closer to the target value than step size then just set to it
    if(std::abs(in->linear.x - out.linear.x) < maxLinXStep){  
        out.linear.x = in->linear.x;
    } 
    else{  //Else increase it the step size
      if(in->linear.x > out.linear.x) out.linear.x += maxLinXStep;
      else out.linear.x -= maxLinXStep;
    }

    //Same for linear translation
    if(std::abs(in->linear.y - out.linear.y) < maxLinYStep){  
        out.linear.y = in->linear.y;
    } 
    else{  
      if(in->linear.y > out.linear.y) out.linear.y += maxLinYStep;
      else out.linear.y -= maxLinYStep;
    }

    //Same for rotation
    if(std::abs(in->angular.z - out.angular.z) < maxRotZStep){  
        out.angular.z = in->angular.z;
    } 
    else{  
      if(in->angular.z > out.angular.z) out.angular.z += maxRotZStep;
      else out.angular.z -= maxRotZStep;
    }

}

void twistCallback(const geometry_msgs::Twist::ConstPtr& msg)
{

  //Take acceleration limits into account
  TwistTowardTwist(msg,limOutTwist);
  //Converts the twist command to the four wheel angles and speeds for the mobile base
  VectorDrive(driveCmd, limOutTwist);

}

int main(int argc, char **argv){
  ros::init(argc, argv, "quadrifoglio_basedrive_node");
  ros::NodeHandle n;
  //ros::Rate loop_rate(30);
  driveCmd.frontLeftServo = 0.0;
  driveCmd.frontRightServo = 0.0;
  driveCmd.rearLeftServo = 0.0;
  driveCmd.rearRightServo = 0.0;
  driveCmd.frontLeftMot = 0.0;
  driveCmd.frontRightMot = 0.0;
  driveCmd.rearLeftMot = 0.0;
  driveCmd.rearRightMot = 0.0;

  cmdVelTimestamp = ros::Time::now()-ros::Duration(10);

  baseDrivePub = n.advertise<quadrifoglio_msgs::baseDriveCmd>("quadrifoglio/baseDriveCmd", 1);
  ros::Subscriber twistSub = n.subscribe("cmd_vel", 2, twistCallback);


  ros::spin();

  return 0;
}
